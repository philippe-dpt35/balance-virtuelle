Application de simulation d'une balance Roberval permettant de s'exercer à la pesée d'objets.

L'application peut être testée en ligne [ici](https://primtux.fr/applications/balance-virtuelle/index.html)
